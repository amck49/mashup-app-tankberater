# WBS Tankberater Mashup-App:

 * Es handelt sich um eine Mashup-App, die Tankstellendaten und Wetterdaten kombiniert.

####Mithilfe der Tankstellendaten bekommen Sie eine Übersicht:

 * Wie teuer der Sprit der nächstgelegenen Tankstelle aktuell ist
 * Für wie viel Geld Sie wie viel Liter
 * Für wie viel Liter Sie wie viel Geld ausgeben müssen
 * Welche Adresse Sie anfahren müssen
 * Wie weit Sie fahren müssen und ob Ihr Tank bis zur Tankstelle reicht
 
####Und kombiniert mit den Wetterdaten können wir Ihnen wichtige Warnhinweise geben:
 
 * Wenn Glatteisgefahr aufgrund der Wetterlage besteht und Sie wegen großer Entfernung zur Tankstelle später fahren oder vorsichtig fahren sollten
 * Wenn Ihr Auto Starthilfe benötigen könnte aufgrund zu niedrigen Temperaturen
 * Ob Sie die Fahrt zur Tankstelle bei niedrigen Temperaturen dem Motor schaden könnte und Sie die Kurzstreckenfahrt vermeiden sollten.
 * Wenn Sie aufgrund zu niedrigen Temperaturen Ihr Auto nicht waschen sollten
 * Ob Ihr Auto beim Fahren langer Strecken überhitzen könnte. Zum Beispiel bei Fahrten im Gebirge oder mit hoher Drehzahl.
 * Wenn Sie wenig Sprit im Tank haben und besser einen Reservekanister mitnehmen sollten, um Ihr Ziel zu erreichen bzw. die Tankstelle zu erreichen.
 * Ob Überhitzungsgefahr bei diesen Temperaturen besteht, wenn sie viele Höhenmeter bergauf fahren möchten oder mit hoher Drehzahl fahren möchten. 

##Information zu den verwendeten Farben:

 * [Die Farben sind von Google's Material Design Color System](https://material.io/design/color/the-color-system.html#tools-for-picking-colors)
 * Ich verwende Light Blue 50 und von dort die empfohlenen Farben (Siehe Video weiter oben) 500 mit dem Wert #03A9F4 und 700 mit dem Wert #0288D1
 
##Installationsanleitung:

  1. [Downloaden Sie auf Gitlab das Repo](https://git.thm.de/amck49/mashup-app-tankberater) oder Klonen mit SSH Sie das Repo: git clone git@git.thm.de:amck49/mashup-app-tankberater.git
  2. Starten Sie die app.js mit Ihrer Entwicklungsumgebung
  3. Öffnen Sie Ihren Webbrowser und geben Sie [localhost:5000](localhost:5000) in der URL-Adresszeile ein
  4. Bearbeiten Sie die Website mit Ihrer Entwicklungsumgebung im Unterverzeichnis "public"
  5. npm install
  6. npm start