console.log("0");
const iconElement = document.querySelector(".weather-icon");
const tempElement = document.querySelector(".temperature-value p");
const descElement = document.querySelector(".temperature-description p");
const locationElement = document.querySelector(".location p");
const locationsElement = document.querySelector(".locations p");
const notificationElement = document.querySelector(".notification");


const weather = {};

weather.temperature = {
    unit : "celsius"
}


const KELVIN = 273;

const key = "e1c722379c86566ca86542ce809d3d16";


if('geolocation' in navigator){
    navigator.geolocation.getCurrentPosition(setPosition, showError);
}else{
    notificationElement.style.display = "block";
    notificationElement.innerHTML = "<p>Dein Browser unterstützt keine Geolocation</p>";
}


function setPosition(position){
    let latitude = position.coords.latitude;
    let longitude = position.coords.longitude;

    getWeather(latitude, longitude);
    setPositionT(position);
}

function setPositionT(position){
    let latitudeT = position.coords.latitude;
    let longitudeT = position.coords.longitude;
    console.log({latitudeT});
    console.log({longitudeT});

    getTank(latitudeT, longitudeT);
}


function showError(error){
    notificationElement.style.display = "block";
    notificationElement.innerHTML = `<p> ${error.message} </p>`;
}


function getWeather(latitude, longitude){
    let api = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${key}`;
    
    fetch(api)
        .then(function(response){
            let data = response.json();
            return data;
        })
        .then(function(data){
            weather.temperature.value = Math.floor(data.main.temp - KELVIN);
            weather.temperature.value2 = Math.floor(data.main.temp - KELVIN);
            weather.temperature.value3 = Math.floor(data.main.temp - KELVIN);
            console.log(weather.temperature.value);
            console.log(weather.temperature.value2);
            weather.description = data.weather[0].description;
            weather.iconId = data.weather[0].icon;
            weather.city = data.name;
            weather.country = data.sys.country;
            weather.citys = data.name;
            weather.countrys = data.weather[0].main;
        })
        .then(function(){
            displayWeather();
        });
}


function displayWeather(){
    iconElement.innerHTML = `<img alt="Weather" src="icons/${weather.iconId}.png"/>`;
    tempElement.innerHTML = `${weather.temperature.value}°<span>C</span>`;
    descElement.innerHTML = weather.description;
    locationElement.innerHTML = `${weather.city}, ${weather.country}`;
    locationsElement.innerHTML = `${weather.citys}, ${weather.countrys}`;
}


function celsiusToFahrenheit(temperature){
    return (temperature * 9/5) + 32;
}

// WHEN THE USER CLICKS ON THE TEMPERATURE ELEMENET
tempElement.addEventListener("click", function(){
    if(weather.temperature.value === undefined) return;
    
    if(weather.temperature.unit == "celsius"){
        let fahrenheit = celsiusToFahrenheit(weather.temperature.value);
        fahrenheit = Math.floor(fahrenheit);
        
        tempElement.innerHTML = `${fahrenheit}°<span>F</span>`;
        weather.temperature.unit = "fahrenheit";
    }else{
        tempElement.innerHTML = `${weather.temperature.value}°<span>C</span>`;
        weather.temperature.unit = "celsius"
    }
});

console.log("1");
const nameTElement = document.querySelector(".nameT p");
const brandElement = document.querySelector(".brand p");
const streetElement = document.querySelector(".street p");
const placeElement = document.querySelector(".place p");
const distElement = document.querySelector(".dist p");
const dieselElement = document.querySelector(".diesel");
const e5Element = document.querySelector(".e5");
const e10Element = document.querySelector(".e10");
const isOpenElement = document.querySelector(".isOpen");
const houseNumberElement = document.querySelector(".houseNumber");
const postCodeElement = document.querySelector(".postCode");
const tank = {};




function getTank(latitudeT, longitudeT) {
    console.log("2");
    let api = `https://creativecommons.tankerkoenig.de/json/list.php?lat=${latitudeT}&lng=${longitudeT}&rad=15&sort=dist&type=all&apikey=a3679ac3-1200-bd07-1a71-f1d2df7c10ae`;
    console.log("3");
    fetch(api)

        .then(function (response) {
            let data = response.json();
            return data;
        })
        .then(function (data) {
            tank.nameT = data.stations[0].name;
            tank.nameT2 = data.stations[0].name;
            tank.nameT3 = data.stations[0].name;
            tank.brandT = data.stations[0].brand;
            tank.streetT = data.stations[0].street;
            tank.placeT = data.stations[0].place;
            tank.distT = data.stations[0].dist;
            tank.distT2 = data.stations[0].dist;
            tank.distT3 = data.stations[0].dist;
            tank.dieselT = data.stations[0].diesel;
            tank.dieselT2 = data.stations[0].diesel;
            tank.dieselT3 = data.stations[0].diesel;
            tank.e5T = data.stations[0].e5;
            tank.e5T2 = data.stations[0].e5;
            tank.e5T3 = data.stations[0].e5;
            tank.e10T = data.stations[0].e10;
            tank.e10T2 = data.stations[0].e10;
            tank.e10T3 = data.stations[0].e10;
            tank.isOpenT = data.stations[0].isOpen;
            tank.houseNumberT = data.stations[0].houseNumber;
            tank.postCodeT = data.stations[0].postCode;
            tank.postCodeT2 = data.stations[0].postCode;
        })
        .then(function () {
            displayTank();
        });
}

function displayTank(){
    console.log("4");
    nameTElement.innerHTML = tank.nameT;
    brandElement.innerHTML = tank.brandT;
    streetElement.innerHTML = tank.streetT;
    placeElement.innerHTML = tank.placeT;
    distElement.innerHTML = `${tank.distT} Kilometer`;
    dieselElement.innerHTML = tank.dieselT;
    e5Element.innerHTML = tank.e5T;
    e10Element.innerHTML = tank.e10T
    isOpenElement.innerHTML = tank.isOpenT;
    houseNumberElement.innerHTML = tank.houseNumberT;
    postCodeElement.innerHTML = tank.postCodeT;

}

function dieselcalculate() {
    var num1 = parseInt(document.getElementById("num1").value);
    var dieselK = num1 / tank.dieselT2;
    var dieselK4 = Number(dieselK).toFixed(1);

    if(num1<=0){
        document.getElementById("output3").innerHTML = `Für 0€ werden Sie nicht tanken können :D`
    }

    else if(num1>=2000){
        document.getElementById("output3").innerHTML = `Sie wollen wirklich für mehr als 2000€ tanken? :D`
    }

    else if (num1){
        document.getElementById("output3").innerHTML = `Für ${num1}€ können Sie ${dieselK4} Liter Diesel tanken.`
    }

    else {
        document.getElementById("output3").innerHTML = `Es konnte nichts berechnet werden. Bitte prüfen Sie Ihre Eingabe nochmals oder laden Sie die Seite bitte neu, da die API möglicherweise die Tankdaten aufgrund zu häufiger Abfrage kurzfristig gesperrt hat.`
    }
}

function supercalculate() {
    var num1 = parseInt(document.getElementById("num1").value);
    var superK= num1 / tank.e5T2;
    var superK4 = Number(superK).toFixed(1);

    if(num1<=0){
        document.getElementById("output3").innerHTML = `Für 0€ werden Sie nicht tanken können :D`
    }

    else if(num1>=2000){
        document.getElementById("output3").innerHTML = `Sie wollen wirklich für mehr als 2000€ tanken? :D`
    }

    else if (num1){
        document.getElementById("output3").innerHTML = `Für ${num1}€ können Sie ${superK4} Liter Super tanken.`
    }

    else {
        document.getElementById("output3").innerHTML = `Es konnte nichts berechnet werden. Bitte prüfen Sie Ihre Eingabe nochmals oder laden Sie die Seite bitte neu, da die API möglicherweise die Tankdaten aufgrund zu häufiger Abfrage kurzfristig gesperrt hat.`
    }
}

function e10calculate() {
    var num1 = parseInt(document.getElementById("num1").value);
    var E10K = num1 / tank.e10T2;
    var E10K4 = Number(E10K).toFixed(1);


    if(num1<=0){
        document.getElementById("output3").innerHTML = `Für 0€ werden Sie nicht tanken können :D`
    }

    else if(num1>=2000){
        document.getElementById("output3").innerHTML = `Sie wollen wirklich für mehr als 2000€ tanken? :D`
    }

    else if (num1){
        document.getElementById("output3").innerHTML = `Für ${num1}€ können Sie ${E10K4} Liter Super E10 tanken.`
    }

    else {
        document.getElementById("output3").innerHTML = `Es konnte nichts berechnet werden. Bitte prüfen Sie Ihre Eingabe nochmals oder laden Sie die Seite bitte neu, da die API möglicherweise die Tankdaten aufgrund zu häufiger Abfrage kurzfristig gesperrt hat.`
    }
}


function dieselLcalculate() {
    var num2 = parseInt(document.getElementById("num2").value);
    var dieselK2 = tank.dieselT3 * num2;
    var dieselK3 = Number(dieselK2).toFixed(2);

    if(num2<=0){
        document.getElementById("output4").innerHTML = `Weniger als 0 Liter werden Sie nicht tanken können :D`
    }

    else if(num2>=2000){
        document.getElementById("output4").innerHTML = `Sie wollen wirklich für mehr als 2000€ tanken? :D`
    }

    else if (num2){
        document.getElementById("output4").innerHTML = `Für ${num2} Liter Diesel zahlen Sie ${dieselK3}€.`
    }

    else {
        document.getElementById("output4").innerHTML = `Es konnte nichts berechnet werden. Bitte prüfen Sie Ihre Eingabe nochmals oder laden Sie die Seite bitte neu, da die API möglicherweise die Tankdaten aufgrund zu häufiger Abfrage kurzfristig gesperrt hat.`
    }
}

function superLcalculate() {
    var num2 = parseInt(document.getElementById("num2").value);
    var superK2 = tank.e5T3 * num2;
    var superK3 = Number(superK2).toFixed(2);

    if(num2<=0){
        document.getElementById("output4").innerHTML = `Weniger als 0 Liter werden Sie nicht tanken können :D`
    }

    else if(num2>=2000){
        document.getElementById("output4").innerHTML = `Sie wollen wirklich für mehr als 2000€ tanken? :D`
    }

    else if (num2){
        document.getElementById("output4").innerHTML = `Für ${num2} Liter Super zahlen Sie ${superK3}€.`
    }

    else {
        document.getElementById("output4").innerHTML = `Es konnte nichts berechnet werden. Bitte prüfen Sie Ihre Eingabe nochmals oder laden Sie die Seite bitte neu, da die API möglicherweise die Tankdaten aufgrund zu häufiger Abfrage kurzfristig gesperrt hat.`
    }
}

function e10Lcalculate() {
    var num2 = parseInt(document.getElementById("num2").value);
    var E10K2 = tank.e10T3 * num2;
    var E10K3 = Number(E10K2).toFixed(2);


    if(num2<=0){
        document.getElementById("output4").innerHTML = `Weniger als 0 Liter werden Sie nicht tanken können :D`
    }

    else if(num2>=2000){
        document.getElementById("output4").innerHTML = `Sie wollen wirklich für mehr als 2000€ tanken? :D`
    }

    else if (num2){
        document.getElementById("output4").innerHTML = `Für ${num2} Liter Super E10 zahlen Sie ${E10K3}€.`
    }

    else {
        document.getElementById("output4").innerHTML = `Es konnte nichts berechnet werden. Bitte prüfen Sie Ihre Eingabe nochmals oder laden Sie die Seite bitte neu, da die API möglicherweise die Tankdaten aufgrund zu häufiger Abfrage kurzfristig gesperrt hat.`
    }
}


function weathercalculate() {
    var tempcalculate = weather.temperature.value2;

    if (tempcalculate>24){
        document.getElementById("output").innerHTML = `Es sind gerade ${weather.temperature.value2}°<span>C</span> draußen, also sehr heiß. Ihr Auto wird vermutlich starten, aber beim Fahren könnte es überhitzen. Die nächste Tankstelle ist die ${tank.nameT2} und ${tank.distT2} Kilometer entfernt.`
    }

    else if (tempcalculate<-30){
        document.getElementById("output").innerHTML = `Es sind gerade ${weather.temperature.value2}°<span>C</span> draußen. Es ist sehr kalt draußen! Ihr Auto wird vermutlich nicht anspringen. Wir empfehlen jetzt nicht tanken zu gehen und Ihr Auto bei den Temperaturen nicht zu waschen.`
    }

    else if (tempcalculate<-10){
        document.getElementById("output").innerHTML = `Es sind gerade ${weather.temperature.value2}°<span>C</span> draußen. Es ist ziemlich kalt draußen! Es kann sein, dass Ihr Auto Starthilfe benötigt. Die nächste Tankstelle ist die ${tank.nameT2} und ${tank.distT2} Kilometer entfernt. Fahren Sie vorsichtig.`
    }

    else if (tempcalculate<1){
        document.getElementById("output").innerHTML = `Es sind gerade ${weather.temperature.value2}°<span>C</span> draußen. Achtung es herscht Glatteisgefahr! Es kann sein, dass Ihr Auto Starthilfe benötigt. Die nächste Tankstelle ist die ${tank.nameT2} und ${tank.distT2} Kilometer entfernt. Sie sollten Ihr Auto bei diesen Temperaturen nicht waschen.`
    }

    else if (tempcalculate>15){
        document.getElementById("output").innerHTML = `Es sind gerade ${weather.temperature.value2}°<span>C</span> draußen, also angenehm warm. Ihr Auto wird sicherlich starten und beim Fahren nicht zu heiß werden. Die nächste Tankstelle ist die ${tank.nameT2} und ${tank.distT2} Kilometer entfernt.`
    }

    else if (tempcalculate>0){
        document.getElementById("output").innerHTML = `Es sind gerade ${weather.temperature.value2}°<span>C</span> draußen, also recht kühl. Ihr Auto wird sicherlich starten und beim Fahren nicht zu heiß werden. Die nächste Tankstelle ist die ${tank.nameT2} und ${tank.distT2} Kilometer entfernt. Sie können Ihr Auto bei den Temperaturen noch waschen.`
    }

    else if (tempcalculate<25){
        document.getElementById("output").innerHTML = `Es sind gerade ${weather.temperature.value2}°<span>C</span> draußen, also angenehm warm. Ihr Auto wird sicherlich starten und beim Fahren nicht zu heiß werden. Die nächste Tankstelle ist die ${tank.nameT2} und ${tank.distT2} Kilometer entfernt.`
    }

    else{
        document.getElementById("output").innerHTML = ("Es konnten keine Wetterdaten ermittelt werden. Bitte lassen Sie den Standort zu.");
    }
}

function weatherTcalculate() {
    var tempTcalculate = weather.temperature.value2;

    if (tempTcalculate>24){
        document.getElementById("output6").innerHTML = `Bei ${weather.temperature.value2}°<span>C</span> draußen können Sie bedenkenlos fahren. Kurzstreckenfahrten sind zwar generell schlecht für den Motor und für die Umwelt, aber aufgrund der hohen Temperatur schaden Sie dem Motor weniger bis zur Tankstelle ${tank.nameT3}. Hin und zurück fahren Sie ${tank.distT2*2} Kilometer. Das ist in Ordnung.`
    }

    else if (tempTcalculate<-30){
        document.getElementById("output6").innerHTML = `Bei ${weather.temperature.value2}°<span>C</span> draußen sollten Sie nicht fahren! Ihr Auto wird vermutlich nicht anspringen. Wir empfehlen jetzt nicht tanken zu fahren.`
    }

    else if (tempTcalculate<-10){
        document.getElementById("output6").innerHTML = `Bei ${weather.temperature.value2}°<span>C</span> draußen sollten Sie nicht fahren! Es kann sein, dass Ihr Auto außerdem Starthilfe benötigt.`
    }

    else if (tempTcalculate<1){
        document.getElementById("output6").innerHTML = `Bei ${weather.temperature.value2}°<span>C</span> draußen sollten sie es sich überlegen zu fahren! Es kann sein, dass Ihr Auto Starthilfe benötigt. Die nächste Tankstelle ist die ${tank.nameT3} Kurzstreckenfahrten sind zwar generell schlecht für den Motor und für die Umwelt, aber aufgrund der aktuellen Temperatur schaden Sie dem Motor weniger. Hin und zurück fahren Sie ${tank.distT2*2} Kilometer. Das ist in Ordnung.`
    }

    else if (tempTcalculate>15){
        document.getElementById("output6").innerHTML = `Bei ${weather.temperature.value2}°<span>C</span> draußen können Sie fahren. Kurzstreckenfahrten sind zwar generell schlecht für den Motor und für die Umwelt, aber aufgrund der aktuellen Temperatur schaden Sie dem Motor weniger bis zur Tankstelle ${tank.nameT3}. Hin und zurück fahren Sie ${tank.distT2*2} Kilometer. Das ist in Ordnung.`
    }

    else if (tempTcalculate>0){
        document.getElementById("output6").innerHTML = `Bei ${weather.temperature.value2}°<span>C</span> draußen können Sie fahren. Kurzstreckenfahrten sind zwar generell schlecht für den Motor und für die Umwelt, aber aufgrund der aktuellen Temperatur schaden Sie dem Motor weniger bis zur Tankstelle ${tank.nameT3}. Hin und zurück fahren Sie ${tank.distT2*2} Kilometer. Das ist in Ordnung.`
    }

    else if (tempTcalculate<25){
        document.getElementById("output6").innerHTML = `Bei ${weather.temperature.value2}°<span>C</span> draußen, können Sie fahren. Kurzstreckenfahrten sind zwar generell schlecht für den Motor und für die Umwelt, aber aufgrund der hohen Temperatur schaden Sie dem Motor weniger bis zur Tankstelle ${tank.nameT3}. Hin und zurück fahren Sie ${tank.distT2 * 2} Kilometer. Das ist in Ordnung.`
    }

    else{
        document.getElementById("output6").innerHTML = ("Es konnten keine Wetterdaten ermittelt werden. Bitte lassen Sie den Standort zu.");
    }
}

function streckecalc() {
    var TempZcalculate = weather.temperature.value3;

    if (TempZcalculate>24){
        document.getElementById("output5").innerHTML = `Es besteht Überhitzungsgefahr für Ihren Motor! Es ist mit ${weather.temperature.value3}°<span>C</span> sehr heiß draußen. Meiden Sie hohe Motorbelastungen.`
    }

    else if (TempZcalculate<-30){
        document.getElementById("output5").innerHTML = `Es besteht keine Überhitzungsgefahr für Ihren Motor. Es ist mit ${weather.temperature.value3}°<span>C</span> sehr kalt draußen.`
    }

    else if (TempZcalculate<-10){
        document.getElementById("output5").innerHTML = `Es besteht keine Überhitzungsgefahr für Ihren Motor. Es ist mit ${weather.temperature.value3}°<span>C</span> sehr kalt draußen.`
    }

    else if (TempZcalculate<1){
        document.getElementById("output5").innerHTML = `Es besteht keine Überhitzungsgefahr für Ihren Motor. Es ist mit ${weather.temperature.value3}°<span>C</span> kalt draußen.`
    }

    else if (TempZcalculate>15){
        document.getElementById("output5").innerHTML = `Es besteht keine Überhitzungsgefahr für Ihren Motor. Es ist mit ${weather.temperature.value3}°<span>C</span> angenehm warm draußen.`
    }

    else if (TempZcalculate>0){
        document.getElementById("output5").innerHTML = `Es besteht keine Überhitzungsgefahr für Ihren Motor. Es ist mit ${weather.temperature.value3}°<span>C</span> recht kühl draußen.`
    }

    else if (TempZcalculate<25){
        document.getElementById("output5").innerHTML = `Es besteht keine Überhitzungsgefahr für Ihren Motor. Es ist mit ${weather.temperature.value3}°<span>C</span> angenehm warm draußen.`
    }

    else{
        document.getElementById("output5").innerHTML = ("Es konnten keine Wetterdaten ermittelt werden. Bitte lassen Sie den Standort zu.");
    }
}

function tankcalculate() {
    var num3 = parseInt(document.getElementById("num3").value);
    var num4 = parseInt(document.getElementById("num4").value);
    var num5 = num3/6;
    var num6 = num3/18;
    var num7 = num4*3;
    console.log(num7);

    if (num3==0){
        document.getElementById("output2").innerHTML = `Wenn Sie nicht fahren möchten müssen, Sie natürlich auch nicht tanken :D`
    }

    else if (num4>=2000){
        document.getElementById("output2").innerHTML = `Meinen Sie nicht auch, dass eine solche Tankgröße ein bisschen unrealistisch ist? Aber seien Sie versichert, mit so vielen Litern kommen Sie sicher dort an, wo sie hinmöchten :D`
    }

    else if (num4==0){
        document.getElementById("output2").innerHTML = `Ihr Tank ist fast leer. Wir empfehlen zur Sicherheit einen Reservekanister mitzunehmen. Die nächste Tankstelle ist die ${tank.nameT2} und ${tank.distT2} Kilometer entfernt.`
    }

    else if (num4<=0){
        document.getElementById("output2").innerHTML = `Weniger also 0 Liter im Tank? Wie soll das gehen? :D`
    }

    else if (num3>=40000){
        document.getElementById("output2").innerHTML = `Wie oft möchten Sie die Erde umrunden? :D`
    }

    else if (num3<=-1){
        document.getElementById("output2").innerHTML = `Wollen Sie rückwärts fahren? Ihre Rechnung wird nicht aufgehen :D`
    }

    else if (num7>=num3){
        document.getElementById("output2").innerHTML = `Ihr Tank wird sicherlich reichen!`
    }

    else if(num4>=num5){
        document.getElementById("output2").innerHTML = `Ihr Tank müsste reichen. Wir wünschen Ihnen eine gute Fahrt!`
    }

    else if (num6>=num4){
        document.getElementById("output2").innerHTML = `Sie sollten tringend nochmal tanken, aber bis zur nächsten Tankstelle ${tank.nameT2} in ${tank.distT2} Kilometern Entfernung kommen Sie noch.`
    }

    else if (num5>=num4){
        document.getElementById("output2").innerHTML = `Sie sollten nochmal tanken. Wir empfehlen Ihnen die Tankstelle ${tank.nameT2} in ${tank.distT2} Kilometern Entfernung.`
    }

    else{
        document.getElementById("output2").innerHTML = `Sie haben eine ungültige Eingabe getätigt oder etwas vergessen einzugeben!`
    }



}